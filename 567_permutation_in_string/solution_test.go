package permutationinstring

import "testing"

func TestMaxVowels(t *testing.T) {
	t.Parallel()
	type input struct {
		s1 string
		s2 string
	}
	exp1 := input{
		s1: "ab",
		s2: "eidbaooo",
	}
	exp2 := input{
		s1: "ab",
		s2: "eidboaoo",
	}
	exp3 := input{
		s1: "adc",
		s2: "dcda",
	}
	exp4 := input{
		s1: "hello",
		s2: "ooolleoooleh",
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    bool
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    true,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    false,
		},
		{
			name:        "example 3",
			description: "test 3",
			input:       exp3,
			expected:    true,
		},
		{
			name:        "example 4",
			description: "test 4",
			input:       exp4,
			expected:    false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := checkInclusion(tc.input.s1, tc.input.s2)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
