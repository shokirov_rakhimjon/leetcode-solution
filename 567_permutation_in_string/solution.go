package permutationinstring

// 567. Permutation in String

/*
Given two strings s1 and s2, return true if s2 contains
a permutation of s1, or false otherwise.

In other words, return true if one of s1's permutations
is the substring of s2.
*/

// s1 = "ab", s2 = "eidbaooo" output=true
func checkInclusion(s1 string, s2 string) bool {
	wLen := len(s1)
	index := 0
	chars1, chars2 := [26]byte{}, [26]byte{}
	for i := 0; i < len(s1); i++ {
		chars1[s1[i]-'a']++
	}
	for i := 0; i < len(s2); i++ {
		chars2[s2[i]-'a']++
		if i-index+1 == wLen {
			if compare(chars1, chars2) {
				return true
			}
			chars2[s2[index]-'a']--
			index++
		}
	}
	return false
}

func compare(chars1, chars2 [26]byte) bool {
	for i := 0; i < 26; i++ {
		if chars1[i] != chars2[i] {
			return false
		}
	}
	return true
}
