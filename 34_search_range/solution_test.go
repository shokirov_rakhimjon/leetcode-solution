package search_range

import (
	"slices"
	"testing"
)

func TestSearchRange(t *testing.T) {
	t.Parallel()
	type input struct {
		nums   []int
		target int
	}
	exp1 := input{
		nums:   []int{5, 7, 7, 8, 8, 10},
		target: 8,
	}
	exp2 := input{
		nums:   []int{5, 7, 7, 8, 8, 10},
		target: 6,
	}
	exp3 := input{
		nums:   []int{},
		target: 0,
	}
	exp4 := input{
		nums:   []int{1},
		target: 1,
	}

	testCases := []struct {
		name     string
		input    input
		expected []int
	}{
		{
			name:     "example 1",
			input:    exp1,
			expected: []int{3, 4},
		},
		{
			name:     "example 2",
			input:    exp2,
			expected: []int{-1, -1},
		},
		{
			name:     "example 3",
			input:    exp3,
			expected: []int{-1, -1},
		},
		{
			name:     "example 4",
			input:    exp4,
			expected: []int{0, 0},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := searchRange(tc.input.nums, tc.input.target)

			if !slices.Equal(got, tc.expected) {
				t.Fatalf("expected %v, got %v.", tc.expected, got)
			}
		})
	}
}
