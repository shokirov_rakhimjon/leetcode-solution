package search_range

// 34. Find First and Last Position of Element in Sorted Array
// https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/description/

func searchRange(nums []int, target int) []int {
	ans := []int{-1, -1}

	start := search(nums, target, true)
	end := search(nums, target, false)
	ans[0] = start
	ans[1] = end
	return ans
}

func search(nums []int, target int, findStartIndex bool) int {
	ans := -1
	l, r, m := 0, len(nums)-1, 0
	for l <= r {
		m = l + (r-l)/2
		if nums[m] < target {
			l = m + 1
		} else if nums[m] > target {
			r = m - 1
		} else {
			ans = m
			if findStartIndex {
				r = m - 1
			} else {
				l = m + 1
			}
		}
	}
	return ans
}
