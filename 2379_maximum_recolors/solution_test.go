package maximumrecolors

import "testing"

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		blocks string
		k      int
	}
	exp1 := input{
		blocks: "WBBWWBBWBW",
		k:      7,
	}
	exp2 := input{
		blocks: "WBWBBBW",
		k:      2,
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    3,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    0,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := minimumRecolors(tc.input.blocks, tc.input.k)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
