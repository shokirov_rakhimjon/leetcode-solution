package maximumrecolors

/*
2379. Minimum Recolors to Get K Consecutive Black Blocks

You are given a 0-indexed string blocks of length n, where blocks[i] is either 'W' or 'B',
representing the color of the ith block. The characters 'W' and 'B' denote the colors
white and black, respectively.

You are also given an integer k, which is the desired number of consecutive black blocks.

In one operation, you can recolor a white block such that it becomes a black block.

Return the minimum number of operations needed such that there is at least one occurrence
of k consecutive black blocks.
*/

// blocks = "WBBWWBBWBW", k = 7 output=3
func minimumRecolors(blocks string, k int) int {
	var (
		index int
		sum   int = 101
		str   string
	)
	for i := 0; i < len(blocks); i++ {
		str += string(blocks[i])
		if i-index+1 == k {
			if a := testFunc(str, k); sum > a {
				sum = a
			}
			str = str[1:]
			index++
		}
	}
	return sum
}

func testFunc(str string, k int) int {
	resp := 0
	for _, v := range str {
		if v == 'W' {
			resp++
		}
	}
	return resp
}
