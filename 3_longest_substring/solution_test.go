package longestsubstring

import "testing"

func TestMaxVowels(t *testing.T) {
	t.Parallel()
	type input struct {
		blocks string
	}
	exp1 := input{
		blocks: "abcabcbb",
	}
	exp2 := input{
		blocks: "bbbbb",
	}
	exp3 := input{
		blocks: "pwwkew",
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    3,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    1,
		},
		{
			name:        "example 3",
			description: "test 3",
			input:       exp3,
			expected:    3,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := lengthOfLongestSubstring(tc.input.blocks)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
