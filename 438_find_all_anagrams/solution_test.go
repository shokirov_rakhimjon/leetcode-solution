package findallanagrams

import (
	"reflect"
	"testing"
)

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		s string
		p string
	}
	exp1 := input{
		s: "cbaebabacd",
		p: "abc",
	}
	exp2 := input{
		s: "abab",
		p: "ab",
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    []int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    []int{0, 6},
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    []int{0, 1, 2},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := findAnagrams(tc.input.s, tc.input.p)
			if !reflect.DeepEqual(got, tc.expected) {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
