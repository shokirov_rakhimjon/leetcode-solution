package findallanagrams

/*
438. Find All Anagrams in a String

YGiven two strings s and p, return an array of all the start indices
of p's anagrams in s. You may return the answer in any order.

An Anagram is a word or phrase formed by rearranging the letters of
a different word or phrase, typically using all the original letters
exactly once.
*/

// s = "cbaebabacd", p = "abc" output=[0,6]
func findAnagrams(s string, p string) []int {
	var (
		index int
		str   string
		resp  []int
	)
	for i := 0; i < len(s); i++ {
		str += string(s[i])
		if i-index == len(p)-1 {
			if isAnagram(str, p) {
				resp = append(resp, i+1-len(p))
			}
			str = str[1:]
			index++
		}

	}
	return resp
}

func isAnagram(str, p string) bool {
	var (
		hasMap = make([]byte, 26)
	)
	for i := 0; i < len(str); i++ {
		hasMap[p[i]-97]++
		hasMap[str[i]-97]--
	}
	for _, v := range hasMap {
		if v > 0 {
			return false
		}
	}
	return true
}
