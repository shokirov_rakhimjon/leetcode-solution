package number_of_subarray

import "testing"

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		nums      []int
		k         int
		threshold int
	}
	exp1 := input{
		nums:      []int{2, 2, 2, 2, 5, 5, 5, 8},
		k:         3,
		threshold: 4,
	}
	exp2 := input{
		nums:      []int{11, 13, 17, 23, 29, 31, 7, 5, 2, 3},
		k:         3,
		threshold: 5,
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    3,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    6,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := numOfSubarrays(tc.input.nums, tc.input.k, tc.input.threshold)

			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
