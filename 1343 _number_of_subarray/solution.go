package number_of_subarray

/*
Given an array of integers arr and two integers k and threshold,
return the number of sub-arrays of size k and average greater
than or equal to threshold.
*/
// arr = [2,2,2,2,5,5,5,8], k = 3, threshold = 4 Output: 3
func numOfSubarrays(arr []int, k int, threshold int) int {
	var resp, sum, index int
	for i := 0; i < len(arr); i++ {
		sum += arr[i]
		if i-index+1 == k {
			if compareAverage(sum, k, threshold) {
				resp++
			}
			sum -= arr[index]
			index++
		}
	}
	return resp
}

func compareAverage(sum int, k int, threshold int) bool {
	return (float64(sum) / float64(k)) >= float64(threshold)
}
