package find_count_odd_even_numbers

/*
1295. Find Numbers with Even Number of Digits

Given an array nums of integers, return how many of them contain
an even number of digits.

Input: nums = [12,345,2,6,7896]
Output: 2
*/
func findNumbers(nums []int) int {
	resp := 0
	for i := 0; i < len(nums); i++ {
		if digits(nums[i])%2 == 0 {
			resp++
		}
	}

	return resp
}

func digits(n int) int {
	count := 0
	for n > 0 {
		n /= 10
		count++
	}
	return count
}
