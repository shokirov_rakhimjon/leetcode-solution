package find_count_odd_even_numbers

import "testing"

func TestFindNumbers(t *testing.T) {
	t.Parallel()
	type input struct {
		nums []int
	}
	exp1 := input{
		nums: []int{12, 345, 2, 6, 7896},
	}
	exp2 := input{
		nums: []int{555, 901, 482, 1771},
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    2,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    1,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := findNumbers(tc.input.nums)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
