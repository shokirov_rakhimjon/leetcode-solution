package search_in_rotated_sorted_array

import (
	"testing"
)

func TestSearchRange(t *testing.T) {
	t.Parallel()
	type input struct {
		nums   []int
		target int
	}
	exp1 := input{
		nums:   []int{4, 5, 6, 7, 0, 1, 2},
		target: 0,
	}
	exp2 := input{
		nums:   []int{4, 5, 6, 7, 0, 1, 2},
		target: -1,
	}

	testCases := []struct {
		name     string
		input    input
		expected int
	}{
		{
			name:     "example 1",
			input:    exp1,
			expected: 4,
		},
		{
			name:     "example 2",
			input:    exp2,
			expected: -1,
		},
		//{
		//	name: "example 3",
		//	input:    exp3,
		//	expected: -1,
		//},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := search(tc.input.nums, tc.input.target)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.", tc.expected, got)
			}
		})
	}
}
