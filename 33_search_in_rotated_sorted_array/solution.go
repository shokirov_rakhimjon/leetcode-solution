package search_in_rotated_sorted_array

func search(nums []int, target int) int {
	p := findPivotInRotatedArray(nums)
	first := bSearch(nums[:p+1], target)
	if first != -1 {
		return first
	}
	second := bSearch(nums[p+1:], target)
	if second == -1 {
		return -1
	}
	return p + 1 + second
}

func findPivotInRotatedArray(arr []int) int {
	s, e, m := 0, len(arr)-1, 0
	for s < e {
		m = s + (e-s)/2
		if arr[m] > arr[e] {
			s = m + 1
		} else {
			e = m
		}
	}
	return s - 1
}

func bSearch(slc []int, target int) int {
	var (
		l = 0
		r = len(slc) - 1
		m = 0
	)

	for l <= r {
		m = l + (r-l)/2
		if slc[m] == target {
			return m
		} else if slc[m] < target {
			l = m + 1
		} else {
			r = m - 1
		}
	}

	return -1
}
