package validparantheses

// 20. Valid Parentheses
/*
Given a string s containing just the characters
'(', ')', '{', '}', '[' and ']', determine if the
input string is valid.

An input string is valid if:

Open brackets must be closed by the same type of brackets.
Open brackets must be closed in the correct order.
Every close bracket has a corresponding open bracket of the same type.
*/

// s = "()[]{}" output=true
func isValid(s string) bool {
	var stack []byte
	for i := range s {
		switch s[i] {
		case '(', '{', '[':
			stack = append(stack, s[i])
		default:
			if len(stack) == 0 {
				return false
			}
			top := stack[len(stack)-1]
			if (top == '(' && s[i] == ')') || (top == '{' && s[i] == '}') || (top == '[' && s[i] == ']') {
				stack = stack[:len(stack)-1]
			} else {
				return false
			}
		}
	}
	return len(stack) == 0
}
