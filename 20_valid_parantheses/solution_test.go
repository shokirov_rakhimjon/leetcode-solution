package validparantheses

import (
	"testing"
)

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		s string
	}
	exp1 := input{
		s: "()",
	}
	exp2 := input{
		s: "()[]{}",
	}
	exp3 := input{
		s: "(]",
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    bool
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    true,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    true,
		},
		{
			name:        "example 3",
			description: "test 3",
			input:       exp3,
			expected:    false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := isValid(tc.input.s)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
