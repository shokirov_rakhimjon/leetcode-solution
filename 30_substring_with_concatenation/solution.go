package substringwithconcatenation

/*
You are given a string s and an array of strings words.
All the strings of words are of the same length.

A concatenated substring in s is a substring that contains
all the strings of any permutation of words concatenated.

For example,
if words = ["ab","cd","ef"], then "abcdef", "abefcd", "cdabef", "cdefab", "efabcd", and "efcdab"
are all concatenated strings. "acdbef" is not a concatenated substring because it is not the
concatenation of any permutation of words.

Return the starting indices of all the concatenated substrings in s.
You can return the answer in any order.
*/

// s = "barfoothefoobarman", words = ["foo","bar"] output=[0,9]
// num = "wordgoodgoodgoodbestword", words = ["word","good","best","word"] output=[]
func findSubstring(s string, words []string) []int {
	var index int
	sum := ""
	resp := []int{}
	k := len(words) * len(words[0])
	for i := 0; i < len(s); i++ {
		sum += string(s[i])
		if (i - index + 1) == k {
			if containsWordConcatenation(sum, words) {
				resp = append(resp, i+1-k)
			}
			sum = sum[1:]
			index++
		}
	}
	return resp
}

func containsWordConcatenation(sum string, words []string) bool {
	ln := len(words[0])
	hashMap := make(map[string]int, 0)
	for i, v := range words {
		hashMap[v]++
		hashMap[sum[i*ln:ln*(i+1)]]--
	}
	for _, v := range hashMap {
		if v != 0 {
			return false
		}
	}
	return true
}
