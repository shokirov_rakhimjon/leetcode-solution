package substringwithconcatenation

import (
	"testing"
)

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		s     string
		words []string
	}
	exp1 := input{
		s:     "barfoothefoobarman",
		words: []string{"foo", "bar"},
	}
	exp2 := input{
		s:     "wordgoodgoodgoodbestword",
		words: []string{"word", "good", "best", "word"},
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    []int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    []int{0, 9},
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    []int{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := findSubstring(tc.input.s, tc.input.words)
			if equal(got, tc.expected) {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}

func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}
