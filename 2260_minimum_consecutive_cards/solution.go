package minimumconsecutivecards

import "math"

// 2260. Minimum Consecutive Cards to Pick Up

/*
You are given an integer array cards where cards[i] represents
the value of the ith card. A pair of cards are matching if the
cards have the same value.

Return the minimum number of consecutive cards you have to pick up
to have a pair of matching cards among the picked cards. If it is
impossible to have matching cards, return -1.
*/

// s =[3,4,2,3,4,7] output=4
// s =[95,11,8,65,5,86,30,27,30,73,15,91,30,7,37,26,55,76,60,43,36,85,47,96,6] output=3
// s=[70,37,70,41,1,62,71,49,38,50,29,76,29,41,22,66,88,18,85,53] output=3
func minimumCardPickup(cards []int) int {
	var l, r int
	resp := math.MaxInt
	hashMap := make(map[int]int, 0)
	for r < len(cards) {
		if a, ok := hashMap[cards[r]]; ok {
			l = a
			resp = min(resp, r-l+1)
		}
		hashMap[cards[r]] = r
		r++
	}
	if resp == math.MaxInt {
		return -1
	}
	return resp
}

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}
