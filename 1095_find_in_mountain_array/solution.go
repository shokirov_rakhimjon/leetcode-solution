package findInMountainArray

import "sync"

type MountainArray struct {
	m     *sync.Mutex
	arr   []int
	count int
}

func (this *MountainArray) get(index int) int {
	this.m.Lock()
	this.count++
	if this.count >= 100 {
		return -1
	}
	this.m.Unlock()

	return this.arr[index]
}

func (this *MountainArray) length() int {
	return len(this.arr)
}

func findInMountainArray(target int, mountainArr *MountainArray) int {
	s, e, m := 0, mountainArr.length()-1, 0
	for s < e {
		m = (s + e) / 2
		cm := mountainArr.get(m)
		if cm == -1 {
			return -1
		}
		cm1 := mountainArr.get(m + 1)
		if cm1 == -1 {
			return -1
		}
		if cm > cm1 {
			e = m
		} else {
			s = m + 1
		}
	}
	// found the peak = s

	l, r := 0, s
	for l <= r {
		m = l + (r-l)/2
		slc_m := mountainArr.get(m)
		if slc_m == -1 {
			return -1
		}
		if slc_m == target {
			return m
		} else if slc_m < target {
			l = m + 1
		} else {
			r = m - 1
		}
	}

	l, r = s+1, mountainArr.length()-1
	for l <= r {
		m = l + (r-l)/2
		slc_m := mountainArr.get(m)
		if slc_m == -1 {
			return -1
		}
		if slc_m == target {
			return m
		} else if slc_m < target {
			r = m - 1
		} else {
			l = m + 1
		}
	}

	return -1
}
