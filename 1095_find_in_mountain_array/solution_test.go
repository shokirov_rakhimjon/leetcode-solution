package findInMountainArray

import (
	"sync"
	"testing"
)

func TestSearchRange(t *testing.T) {
	t.Parallel()
	type input struct {
		nums   []int
		target int
	}
	exp1 := input{
		nums:   []int{1, 2, 3, 4, 5, 3, 1},
		target: 3,
	}
	exp2 := input{
		nums:   []int{0, 1, 2, 8, 2, 1},
		target: 3,
	}
	exp3 := input{
		nums:   []int{0, 2, 1, 0},
		target: 3,
	}

	testCases := []struct {
		name     string
		input    input
		expected int
	}{
		{
			name:     "example 1",
			input:    exp1,
			expected: 2,
		},
		{
			name:     "example 2",
			input:    exp2,
			expected: -1,
		},
		{
			name:     "example 3",
			input:    exp3,
			expected: -1,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			m := &sync.Mutex{}
			arrM := &MountainArray{m, tc.input.nums, 0}
			got := findInMountainArray(tc.input.target, arrM)

			if got != tc.expected {
				t.Fatalf("expected %v, got %v.", tc.expected, got)
			}
		})
	}
}
