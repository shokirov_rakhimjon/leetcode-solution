package maximumnumberofvowels

// 1456. Maximum Number of Vowels in a Substring of Given Length

/*
Given a string s and an integer k, return the maximum number of
vowel letters in any substring of s with length k.

Vowel letters in English are 'a', 'e', 'i', 'o', and 'u'.
*/

// s = "abciiidef", k = 3 output=3
func maxVowels(s string, k int) int {
	tmp, res := 0, 0
	for i := 0; i < k; i++ {
		if isVowels(s[i]) {
			tmp++
		}
	}

	for i := k; i < len(s); i++ {
		if tmp > res {
			res = tmp
		}
		if isVowels(s[i-k]) {
			tmp--
		}
		if isVowels(s[i]) {
			tmp++
		}
	}
	if tmp > res {
		res = tmp
	}
	return res
}

func isVowels(s byte) bool {
	if s == 'a' || s == 'e' || s == 'i' || s == 'o' || s == 'u' {
		return true
	}
	return false
}
