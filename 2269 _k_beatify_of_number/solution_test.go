package kbeatifyofnumber

import "testing"

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		nums int
		k    int
	}
	exp1 := input{
		nums: 240,
		k:    2,
	}
	exp2 := input{
		nums: 430043,
		k:    2,
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    2,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    2,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := divisorSubstrings(tc.input.nums, tc.input.k)

			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
