package kbeatifyofnumber

import "strconv"

/*
The k-beauty of an integer num is defined as the number of substrings of num when
it is read as a string that meet the following conditions:

It has a length of k.
It is a divisor of num.
Given integers num and k, return the k-beauty of num.
*/

// num = 240, k = 2 output=2
// num = 430043, k = 2 output=2
func divisorSubstrings(num int, k int) int {
	str := ""
	index := 0
	resp := 0
	strNum := strconv.Itoa(num)
	for i := 0; i < len(strNum); i++ {
		str += string(strNum[i])
		if i-index+1 == k {
			tmp, _ := strconv.Atoi(str)
			if tmp != 0 && num%tmp == 0 {
				resp++
			}
			str = str[1:]
			index++
		}
	}
	return resp
}
