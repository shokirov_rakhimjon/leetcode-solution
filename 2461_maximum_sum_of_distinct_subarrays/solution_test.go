package maximum_sum_of_distinct_subarrays

import (
	"reflect"
	"testing"
)

type input struct {
	nums []int
	k    int
}

func TestOfStringMatching(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int64
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       input{nums: []int{1, 5, 4, 2, 9, 9, 9}, k: 3},
			expected:    15,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       input{nums: []int{4, 4, 4}, k: 3},
			expected:    0,
		},
		{
			name:        "example 3",
			description: "test 3",
			input:       input{nums: []int{1, 2, 2}, k: 2},
			expected:    3,
		},
		{
			name:        "example 4",
			description: "test 4",
			input:       input{nums: []int{1, 1, 1, 7, 8, 9}, k: 3},
			expected:    24,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := maximumSubarraySum(tc.input.nums, tc.input.k)
			if !reflect.DeepEqual(got, tc.expected) {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
