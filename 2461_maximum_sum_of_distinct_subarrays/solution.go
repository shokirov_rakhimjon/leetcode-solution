package maximum_sum_of_distinct_subarrays

// 2461. Maximum Sum of Distinct Subarrays With Length K

/*
You are given an integer array nums and an integer k. Find the maximum subarray sum of all
the subarrays of nums that meet the following conditions:

The length of the subarray is k, and
All the elements of the subarray are distinct.
Return the maximum subarray sum of all the subarrays that meet the conditions.
If no subarray meets the conditions, return 0.

A subarray is a contiguous non-empty sequence of elements within an array.
*/

// Input: nums = [1, 1, 1, 7, 8, 9], k = 3 Output: 24
func maximumSubarraySum(nums []int, k int) int64 {
	l, r, sum := 0, 0, 0
	var maxSubArray int
	m := map[int]bool{}

	for r < len(nums) {
		if r-l+1 == k {
			if _, ok := m[nums[r]]; ok {
				sum -= nums[l]
				delete(m, nums[l])
				l++
				r++
				continue
			}
			m[nums[r]] = true
			sum += nums[r]

			if sum > maxSubArray {
				maxSubArray = sum
			}
			delete(m, nums[l])
			r++

			sum -= nums[l]
			l++
		} else {
			if _, ok := m[nums[r]]; ok {
				sum -= nums[l]
				if sum > maxSubArray {
					maxSubArray = sum
				}
				delete(m, nums[l])
				l++
				continue
			}
			sum += nums[r]
			m[nums[r]] = true
			r++ // 1
		}
	}
	return int64(maxSubArray)
}
