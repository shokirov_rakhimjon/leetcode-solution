package longestsubstring

import "fmt"

// 3. Longest Substring Without Repeating Characters

/*
Given a string s, find the length of the longest substring
without repeating characters.
*/

// s = "abcabcbb" output=3
// s = "bbbbb"    output=1
// s = "pwwkew"   output=3
func lengthOfLongestSubstring(s string) int {
	m := make(map[rune]int)
	l := 0
	maxLen := 0
	ok := false // this asved 4 ms for total run
	ss := []rune(s)
	for r := 0; r < len(ss); r++ {
		fmt.Println("m[ss[r]]: ", m[ss[r]])
		_, ok = m[ss[r]]
		if ok {
			l = Max(m[ss[r]], l)
		}
		fmt.Println("r: ", r)
		maxLen = Max(maxLen, r-l+1)
		m[ss[r]] = r + 1
	}
	return maxLen
}

func Max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}
