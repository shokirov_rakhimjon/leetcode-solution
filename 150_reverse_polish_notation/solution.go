package reversepolishnotation

import (
	"fmt"
	"strconv"
)

// 20. Valid Parentheses
/*
You are given an array of strings tokens that represents
an arithmetic expression in a Reverse Polish Notation.

Evaluate the expression. Return an integer that represents
the value of the expression.

Note that:
The valid operators are '+', '-', '*', and '/'.
Each operand may be an integer or another expression.
The division between two integers always truncates toward zero.
There will not be any division by zero.
The input represents a valid arithmetic expression in a
reverse polish notation.
The answer and all the intermediate calculations can be
represented in a 32-bit integer.
*/

// tokens = ["2","1","+","3","*"] output=9
func evalRPN(tokens []string) int {
	stack := make([]int, 0)
	var resp int
	if len(tokens) == 1 {
		val, err := strconv.Atoi(tokens[0])
		fmt.Println(val, tokens[0])
		if err == nil {
			return val
		}
	}
	for _, v := range tokens {
		if val, err := strconv.Atoi(v); err == nil {
			stack = append(stack, val)
		} else {
			switch v {
			case "+":
				first := stack[len(stack)-1]
				second := stack[len(stack)-2]
				stack = stack[:len(stack)-2]
				resp = second + first
				stack = append(stack, resp)
			case "-":
				first := stack[len(stack)-1]
				second := stack[len(stack)-2]
				stack = stack[:len(stack)-2]
				resp = second - first
				stack = append(stack, resp)
			case "*":
				first := stack[len(stack)-1]
				second := stack[len(stack)-2]
				stack = stack[:len(stack)-2]
				resp = second * first
				stack = append(stack, resp)
			case "/":
				first := stack[len(stack)-1]
				second := stack[len(stack)-2]
				stack = stack[:len(stack)-2]
				resp = second / first
				stack = append(stack, resp)
			}
		}
	}
	return resp
}
