package reversepolishnotation

import (
	"testing"
)

func TestEvalPRN(t *testing.T) {
	t.Parallel()
	type input struct {
		s []string
	}
	exp1 := input{
		s: []string{"2", "1", "+", "3", "*"},
	}
	exp2 := input{
		s: []string{"4", "13", "5", "/", "+"},
	}
	exp3 := input{
		s: []string{"10", "6", "9", "3", "+", "-11", "*", "/", "*", "17", "+", "5", "+"},
	}
	exp4 := input{
		s: []string{"18"},
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    int
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    9,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    6,
		},
		{
			name:        "example 3",
			description: "test 3",
			input:       exp3,
			expected:    22,
		},
		{
			name:        "example 4",
			description: "test 4",
			input:       exp4,
			expected:    18,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := evalRPN(tc.input.s)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
