package happynumber

import "testing"

func TestDivisorSubstrings(t *testing.T) {
	t.Parallel()
	type input struct {
		n int
	}
	exp1 := input{
		n: 19,
	}
	exp2 := input{
		n: 2,
	}
	testCases := []struct {
		name        string
		description string
		input       input
		expected    bool
	}{
		{
			name:        "example 1",
			description: "test 1",
			input:       exp1,
			expected:    true,
		},
		{
			name:        "example 2",
			description: "test 2",
			input:       exp2,
			expected:    false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			got := isHappy(tc.input.n)
			if got != tc.expected {
				t.Fatalf("expected %v, got %v.\n%s", tc.expected, got, tc.description)
			}
		})
	}
}
