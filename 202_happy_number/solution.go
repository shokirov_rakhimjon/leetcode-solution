package happynumber

/*
202. Happy Number

Write an algorithm to determine if a number n is happy.
A happy number is a number defined by the following process:

Starting with any positive integer, replace the number by the sum of the squares of its digits.
Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a
cycle which does not include 1.

Those numbers for which this process ends in 1 are happy.
Return true if n is a happy number, and false if not.
*/

// n = 19 output=true
func isHappy(n int) bool {
	visited := map[int]struct{}{}
	for n != 1 {
		sumOfSquares := 0
		// compute sum of squares of digits in n
		for n > 0 {
			m := n % 10
			sumOfSquares += m * m
			n = n / 10
		}

		// check if sum is already visited
		if _, ok := visited[sumOfSquares]; ok {
			return false
		}

		// add sum to visited set
		visited[sumOfSquares] = struct{}{}

		// set n to be the sum of squares for next iteration
		n = sumOfSquares
	}
	return true
}
